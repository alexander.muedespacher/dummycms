# 📚 dummyCMS

## Description

DummyCMS is a minimalistic Content Management System (CMS) that serves content through a RESTful API. It's designed to be a simple, lightweight CMS for managing text-based content and serves JSON objects for easy integration with frontends.

## Features

- Language support via API query (`?lang=en`)
- API documentation with Swagger (see /docs)

## Getting Started

### Prerequisites

- Node.js
- npm
- Google Cloud SDK (optional, for deployment)

### Installation

1. Clone the repository
   ```
   git clone https://gitlab.com/alexander.muedespacher/dummycms.git
   ```
2. Navigate to the project folder
   ```
   cd dummycms
   ```
3. Install dependencies
   ```
   npm install
   ```

### Running Locally

To run the project locally, use the following command:

```
npm start
```

### Development Mode with Hot Reload

To start the development server with hot reload, use:

```
npm run dev
```

### Deployment

Deploying to Google Cloud App Engine:

```
gcloud app deploy
```

## API Routes

- GET `/pages`: Fetch all pages
- GET `/pages/:slug`: Fetch single page by slug

## JSON Structure

All files in the `/data` directory will be treated as pages, with the file name serving as the slug for the page. The JSON file for each page should follow the below structure:

```json
{
  "language_code": {
    "id": "unique_page_id",
    "meta": {
      "title": "Page Title",
      "description": "Page Description"
    },
    "components": [
      {
        "type": "component_type",
        "key_1": "value_1",
        "key_2": "value_2"
        // ... additional keys
      }
      // ... additional components
    ]
  }
  // ... additional languages
}
```

- `language_code`: The code representing the language for the page (e.g., `"en"` for English, `"de"` for German).
- `unique_page_id`: A unique identifier for the page.
- `meta`: Metadata for the page including `title` and `description`.
- `components`: An array of components that make up the page.
  - `type`: Specifies the type of component (e.g., `"title"`, `"hero"`, `"textblock"`).
  - `key_1`, `key_2`, etc.: The keys vary based on the component type and contain the respective values for rendering.

This generic structure allows you to add any type of component with its specific keys.

## License

This project is licensed under the ISC License.
