const express = require("express");
const fs = require("fs");
const path = require("path");
const cors = require("cors");

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "DummyCMS API",
      version: "1.0.0",
      description: "API for DummyCMS",
    },
    servers: [{ url: "http://localhost:3000" }],
  },
  apis: ["./server.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

const getPageFiles = () => {
  return fs.readdirSync(path.join(__dirname, "data"));
};

const getJsonFile = (fileName) => {
  const filePath = path.join(__dirname, "data", `${fileName}`);
  if (fs.existsSync(filePath)) {
    return JSON.parse(fs.readFileSync(filePath, "utf8"));
  }
  return null;
};

// Fetch all pages
/**
 * @swagger
 * /pages:
 *   get:
 *     summary: Returns the list of all pages.
 *     description: Fetches a list of all available pages, optionally filtering by language.
 *     parameters:
 *       - in: query
 *         name: lang
 *         schema:
 *           type: string
 *           enum: [en, de]
 *         description: The language to filter pages by.
 *     responses:
 *       200:
 *         description: >
 *           An array of pages. Each object in the array contains the page ID, slug,
 *           and title, among other possible fields.
 *       404:
 *         description: Language not found.
 */
app.get("/pages", (req, res) => {
  const { lang = "en" } = req.query;
  const pageFiles = getPageFiles();
  let pages = pageFiles
    .map((file) => {
      let slug = path.basename(file, ".json");

      slug = `/${slug}`;

      if (slug === "home") {
        slug = "/";
      }

      const pageData = getJsonFile(file);

      if (!pageData || !pageData[lang]) {
        return null;
      }

      return {
        id: pageData[lang].id,
        slug,
        title: pageData[lang].meta.title,
      };
    })
    .filter(Boolean);

  pages = pages.sort((a, b) => a.id - b.id);

  if (pages.length) {
    res.json(pages);
  } else {
    res.status(404).json({ error: "Language not found" });
  }
});

// Fetch a single page by slug
/**
 * @swagger
 * /page/{slug}:
 *   get:
 *     summary: Returns details of a single page.
 *     description: Fetches details of a page identified by its slug, optionally filtering content by language.
 *     parameters:
 *       - in: path
 *         name: slug
 *         required: true
 *         schema:
 *           type: string
 *         description: The slug of the page to fetch.
 *       - in: query
 *         name: lang
 *         schema:
 *           type: string
 *           enum: [en, de]
 *         description: The language to filter the page content by.
 *     responses:
 *       200:
 *         description: >
 *           An object containing details of a single page. This object will include
 *           meta information, and an array of components like title, hero, text block, etc.
 *       404:
 *         description: Page not found or Language not found.
 */
app.get("/page/:slug", (req, res) => {
  const { lang = "en" } = req.query;
  let { slug } = req.params;

  if (slug === "") {
    slug = "home";
  }

  const pageData = getJsonFile(`${slug}.json`);

  if (pageData) {
    if (pageData[lang]) {
      res.json(pageData[lang]);
    } else {
      res.status(404).json({ error: "Language not found" });
    }
  } else {
    res.status(404).json({ error: "Page not found" });
  }
});

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.listen(port, () => {
  console.log(`dummyCMS API running at http://localhost:${port}`);
});
